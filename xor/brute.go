package xor

import (
	"encoding/hex"
	"fmt"
	"unicode/utf8"

	"bitbucket.org/chrisbdaemon/matasano/score"
)

// BruteForceString tries applying each key value supplied to decode an XOR'd string.  Returns the string with the greatest probability of being valid
func BruteForceString(str string, keyChars string) (string, float64, error) {
	var bestSolution string
	bestScore := 0.0

	strBytes, err := hex.DecodeString(str)
	if err != nil {
		return "", 0.0, err
	}

	for _, r := range keyChars {
		key := []byte{byte(r)}
		solutionBytes := Bytes(strBytes, key)

		if !utf8.Valid(solutionBytes) {
			continue
		}

		solutionStr := fmt.Sprintf("%s", solutionBytes)
		score := score.CalculateForString(solutionStr)
		if score > bestScore {
			bestSolution = solutionStr
			bestScore = score
		}
	}

	return bestSolution, bestScore, nil
}
