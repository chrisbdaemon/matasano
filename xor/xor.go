package xor

// Bytes XOR's bytes with the key, one byte at a time.
// If the key isn't long enough, circle around to the front of the key
func Bytes(bytes, key []byte) []byte {
	dstBytes := make([]byte, len(bytes))

	keyIndex := 0
	for i, b := range bytes {
		dstBytes[i] = b ^ key[keyIndex]

		keyIndex++
		if keyIndex > len(key)-1 {
			keyIndex = 0
		}
	}

	return dstBytes
}
