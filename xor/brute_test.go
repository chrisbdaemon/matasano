package xor

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"bitbucket.org/chrisbdaemon/matasano/utility"
)

func TestBruteForceString(t *testing.T) {
	hexStr := "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
	keyChars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

	solutionStr, score, _ := BruteForceString(hexStr, 1, utility.ASCIICharset)

	assert.NotNil(t, solutionStr)
	assert.True(t, score > 0.0)
}
