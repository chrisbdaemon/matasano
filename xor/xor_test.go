package xor

import (
	"encoding/hex"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBytes(t *testing.T) {
	hexStr := "1c0111001f010100061a024b53535009181c"
	keyStr := "686974207468652062756c6c277320657965"
	resStr := "746865206b696420646f6e277420706c6179"

	origBytes, _ := hex.DecodeString(hexStr)
	keyBytes, _ := hex.DecodeString(keyStr)

	bytes := Bytes(origBytes, keyBytes)
	assert.Equal(t, resStr, hex.EncodeToString(bytes))
}
