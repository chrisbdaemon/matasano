package score

import (
	"math"
	"strings"
)

// List of letters we are interested for scoring the string
var LettersOfInterest = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

// Known letter frequency as reported by Cornell's Math Explorers' Club.
// Available at http://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
var KnownLetterFreq = map[rune]float64{
	'A': 8.12, 'B': 1.49, 'C': 2.71, 'D': 4.32, 'E': 12.02, 'F': 2.30,
	'G': 2.03, 'H': 5.92, 'I': 7.31, 'J': 0.10, 'K': 0.69, 'L': 3.98,
	'M': 2.61, 'N': 6.95, 'O': 7.68, 'P': 1.82, 'Q': 0.11, 'R': 6.02,
	'S': 6.28, 'T': 9.10, 'U': 2.88, 'V': 1.11, 'W': 2.09, 'X': 0.17,
	'Y': 2.11, 'Z': 0.07,
}

// CalculateForString takes in a string, generates a score based off differences in letter frequencies from an accepted character frequency table
func CalculateForString(str string) float64 {
	// Get letter freq for string
	freqMap := BuildFreqMap(str)

	// Compare to known letter freq, find difference for each letter and average
	differenceSum := 0.0
	for r, c := range freqMap {
		differenceSum += math.Abs(KnownLetterFreq[r] - c)
	}

	avgDifference := differenceSum / float64(len(freqMap))
	return 100.0 - avgDifference
}

// BuildFreqMap generates a the letter frequency table for a given string
func BuildFreqMap(str string) map[rune]float64 {
	letterCount, totalLetters := countLetters(str)
	freqMap := make(map[rune]float64, len(letterCount))

	for r, c := range letterCount {
		freqMap[r] = 100.0 * (float64(c) / float64(totalLetters))
	}

	return freqMap
}

func countLetters(str string) (map[rune]int, int) {
	str = strings.ToUpper(str)
	totalCount := 0

	freqMap := make(map[rune]int, 0)
	for _, r := range str {
		if strings.ContainsRune(LettersOfInterest, r) {
			if _, ok := freqMap[r]; !ok {
				freqMap[r] = 0
			}
			freqMap[r]++
			totalCount++
		}
	}

	return freqMap, totalCount
}
