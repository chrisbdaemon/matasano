package score

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLetterCount(t *testing.T) {
	str := "Hello world!"
	letterCount, totalLetters := countLetters(str)

	assert.Equal(t, 10, totalLetters)
	assert.Equal(t, 3, letterCount['L'])
}

func TestBuildFreqMap(t *testing.T) {
	str := "Hello world!"
	freqMap := buildFreqMap(str)

	// 'O' appears 20% of the time
	assert.Equal(t, 20, freqMap['O'])
}

func TestCalculateForString(t *testing.T) {
	goodStr := "For a long time it puzzled me how something so expensive, "
	goodStr += "so leading edge, could be so useless. And then it occurred "
	goodStr += "to me that a computer is a stupid machine with the ability "
	goodStr += "to do incredibly smart things, while computer programmers "
	goodStr += "are smart people with the ability to do incredibly stupid "
	goodStr += "things. They are, in short, a perfect match."

	badStr := "HQfrVUVH qcGYEihcICorAPjsYKobr BnkGEPJEpeeNMJWoTQmkvfjZpIaBE"
	badStr += "LVtHPvSVCTgLvtqpmsZjTZzoZOPGTlXGkoNKgXcqutnzmLqQk caQbHcUxtk"
	badStr += "gqoDvKkxLUwBxqFXNqYRGwLmLwvEPPXhGhNfhtVTZfTYgxZOTiezKbS FmAi"
	badStr += "YTalhUPoTWjkfgvKZwRgGwJknyzIzmqFAFPCWTKTZNVWPXUWOThTRkRRCifh"
	badStr += "ESjZxcxLpAtgBMOQWtXrbp colaUCnZuvrcqljigkZmJqmoPMUvkUpFSKVxh"

	goodScore := CalculateForString(goodStr)
	badScore := CalculateForString(badStr)

	assert.True(t, goodScore > badScore)
}
