package main

import (
	"bitbucket.org/chrisbdaemon/matasano/utility"
	"log"
)

func main() {
	hex := "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	base64, err := utility.HexToBase64(hex)
	if err != nil {
		log.Fatal(err)
	}

	hex2, err := utility.Base64ToHex(base64)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Base16: %s\n", hex)
	log.Printf("Base64: %s\n", base64)
	log.Printf("Base16: %s\n", hex2)
}
