package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"bitbucket.org/chrisbdaemon/matasano/utility"
	"bitbucket.org/chrisbdaemon/matasano/xor"
)

func main() {
	data, err := ioutil.ReadFile("strings.txt")
	if err != nil {
		log.Fatal(err)
	}

	bestSolution := ""
	bestScore := 0.0

	str := fmt.Sprintf("%s", data)
	strs := strings.Split(str, "\n")
	for _, str = range strs {
		solution, score, err := xor.BruteForceString(str, 1, utility.AlphaNumCharset)
		if err != nil {
			log.Fatal(err)
		}

		if score > bestScore {
			bestSolution = solution
			bestScore = score
		}
	}

	fmt.Printf("Probable solution found with score %f: %s\n", bestScore, bestSolution)
}
