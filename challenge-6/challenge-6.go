package main

import (
	"bitbucket.org/chrisbdaemon/matasano/utility"
	"fmt"
	"log"
)

func main() {
	str := "HUIfTQsPAh9PE048GmllH0kcDk4TAQsHThsBFkU2AB4BSWQgVB0dQzNTTmVSBgBHVBwNRU0HBAxTEjwMHghJGgkRTxRMIRpHKwAFHUdZEQQJAGQmB1MANxYGDBoXQR0BUlQwXwAgEwoFR08SSAhFTmU+Fgk4RQYFCBpGB08fWXh+amI2DB0PQQ1IBlUaGwAdQnQEHgFJGgkRAlJ6f0kASDoAGhNJGk9FSA8dDVMEOgFSGQELQRMGAEwxX1NiFQYHCQdUCxdBFBZJeTM1CxsBBQ9GB08dTnhOSCdSBAcMRVhICEEATyBUCHQLHRlJAgAOFlwAUjBpZR9JAgJUAAELB04CEFMBJhAVTQIHAh9PG054MGk2UgoBCVQGBwlTTgIQUwg7EAYFSQ8PEE87ADpfRyscSWQzT1QCEFMaTwUWEXQMBk0PAg4DQ1JMPU4ALwtJDQhOFw0VVB1PDhxFXigLTRkBEgcKVVN4Tk9iBgELR1MdDAAAFwoFHww6Ql5NLgFBIg4cSTRWQWI1Bk9HKn47CE8BGwFTQjcEBx4MThUcDgYHKxpUKhdJGQZZVCFFVwcDBVMHMUV4LAcKQR0JUlk3TwAmHQdJEwATARNFTg5JFwQ5C15NHQYEGk94dzBDADsdHE4UVBUaDE5JTwgHRTkAUmc6AUETCgYAN1xGYlUKDxJTEUgsAA0ABwcXOwlSGQELQQcbE0c9GioWGgwcAgcHSAtPTgsAABY9C1VNCAINGxgXRHgwaWUfSQcJABkRRU8ZAUkDDTUWF01jOgkRTxVJKlZJJwFJHQYADUgRSAsWSR8KIgBSAAxOABoLUlQwW1RiGxpOCEtUYiROCk8gUwY1C1IJCAACEU8QRSxORTBSHQYGTlQJC1lOBAAXRTpCUh0FDxhUZXhzLFtHJ1JbTkoNVDEAQU4bARZFOwsXTRAPRlQYE042WwAuGxoaAk5UHAoAZCYdVBZ0ChQLSQMYVAcXQTwaUy1SBQsTAAAAAAAMCggHRSQJExRJGgkGAAdHMBoqER1JJ0dDFQZFRhsBAlMMIEUHHUkPDxBPH0EzXwArBkkdCFUaDEVHAQAN"
	bytes, err := utility.Base64ToBytes(str)
	if err != nil {
		log.Fatal(err)
	}

	lowestDistance := 100.0
	probableKeysize := 0

	for keysize := 2; keysize <= 40; keysize++ {
		distance_sum := 0

		// Use max number of chunks for best result
		chunk_count := len(bytes) / keysize

		// Get get large sample of distances from keysize-len chunks of ciphertext
		// and sum them
		for chnk_n := 1; chnk_n < chunk_count; chnk_n++ {
			chnk1 := bytes[keysize*(chnk_n-1) : keysize*(chnk_n)]
			chnk2 := bytes[keysize*(chnk_n) : keysize*(chnk_n+1)]
			distance_sum += hamming_distance(chnk1, chnk2)
		}

		// Find average distance of samples, normalize against keysize
		normalized_distance := (float64(distance_sum) / float64(chunk_count)) / float64(keysize)
		if normalized_distance < lowestDistance {
			lowestDistance = normalized_distance
			probableKeysize = keysize
		}

	}

	fmt.Printf("Probable keysize: %d\n", probableKeysize)
}

func hamming_distance(bytes1, bytes2 []byte) int {
	distance := 0

	for i, b := range bytes1 {
		xor_val := b ^ bytes2[i]
		if xor_val == 0 {
			continue
		}

		mask := byte(1)
		for j := 0; j < 8; j++ {
			if mask&xor_val != 0 {
				distance++
			}

			mask = mask << 1
		}
	}

	return distance
}
