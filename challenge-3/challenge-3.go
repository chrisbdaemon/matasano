package main

import (
	"fmt"
	"log"

	"bitbucket.org/chrisbdaemon/matasano/utility"
	"bitbucket.org/chrisbdaemon/matasano/xor"
)

func main() {
	str := "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"

	solution, score, err := xor.BruteForceString(str, 1, utility.AlphaNumCharset)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Probable solution found with score %f: %s\n", score, solution)
}
