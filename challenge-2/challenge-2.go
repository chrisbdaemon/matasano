package main

import (
	"encoding/hex"
	"log"

	"bitbucket.org/chrisbdaemon/matasano/xor"
)

func main() {
	origStr := "1c0111001f010100061a024b53535009181c"
	keyStr := "686974207468652062756c6c277320657965"

	origBytes, _ := hex.DecodeString(origStr)
	keyBytes, _ := hex.DecodeString(keyStr)

	xorBytes := xor.Bytes(origBytes, keyBytes)
	xorStr := hex.EncodeToString(xorBytes)

	log.Printf("Original: %s\n", origStr)
	log.Printf("     Key: %s\n", keyStr)
	log.Printf("   XOR'd: %s\n", xorStr)
}
