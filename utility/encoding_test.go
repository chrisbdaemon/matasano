package utility

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHexToBase64(t *testing.T) {
	hex := "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	expected := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	res, err := HexToBase64(hex)

	assert.Equal(t, expected, res)
	assert.NoError(t, err)
}

func TestBase64ToHex(t *testing.T) {
	expected := "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
	base64 := "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
	res, err := Base64ToHex(base64)

	assert.Equal(t, expected, res)
	assert.NoError(t, err)
}
