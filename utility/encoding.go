package utility

import (
	"encoding/base64"
	"encoding/hex"
)

// Base64Charset contains a list of the characters used in Base64 encoding
var Base64Charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

// AlphaNumCharset contains all the letters of the alphabet upper and lower cases and numeric digits
var AlphaNumCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

// ASCIICharset contains all the printable characters in the ascii character set
var ASCIICharset = "\n\r\t !\"#$%&()*+,-.0123456789:;<=>@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^`abcdefghijklmnopqrstuvwxyz{|}~"

// Base64ToBytes is a simple wrapper to streamline use of the Base64Charset in base 64 decoding
func Base64ToBytes(base64Str string) ([]byte, error) {
	encoder := base64.NewEncoding(Base64Charset)
	bytes, err := encoder.DecodeString(base64Str)

	return bytes, err
}

// HexToBase64 converts a string of hex values to base64 using the character set in Base64Charset.
func HexToBase64(hexStr string) (string, error) {
	bytes, err := hex.DecodeString(hexStr)
	if err != nil {
		return "", err
	}

	encoder := base64.NewEncoding(Base64Charset)
	base64Str := encoder.EncodeToString(bytes)

	return base64Str, nil
}

// Base64ToHex converts a base64-encoded string to hex using the character set in Base64Charset.
func Base64ToHex(base64Str string) (string, error) {
	bytes, err := Base64ToBytes(base64Str)
	if err != nil {
		return "", err
	}

	hexStr := hex.EncodeToString(bytes)

	return hexStr, nil
}
