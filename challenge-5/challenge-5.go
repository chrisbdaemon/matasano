package main

import (
	"bitbucket.org/chrisbdaemon/matasano/xor"
	"encoding/hex"
	"fmt"
)

func main() {
	str := []byte("Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal")
	key := []byte("ICE")

	res := hex.EncodeToString(xor.Bytes(str, key))
	fmt.Println(res)
}
